print("Hello World!")

# Types

# Float
f = 10.

# Integer
i = 10

# Bool
b = True  # ou False

# String
s = "teste"  # ou 'teste'

# list
li = [0, 1, 2, 3, True, 'asdf']
print(li[2])
print(li[1:4])

# Dictionary
dic = {'a': 5, 'b': 6}
print(dic['a'])
