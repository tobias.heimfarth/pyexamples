import matplotlib.pyplot as plt


class Waveform:
    def __init__(self, x, y):
        self.x = x  # x deve ser uma lista de valores
        self.y = y  # y dave ser uma lista do mesmo tamanho que x

    def plot(self):
        # Ver como usa em https://matplotlib.org/stable/tutorials/pyplot.html
        plt.plot(self.x, self.y)
        plt.show()

    # ---------

    def _check_compatibility(self, other):
        if self.x != other.x:
            raise Exception("Deu problema, os eixos x não batem!")

    def __add__(self, other):
        ''' Soma duas formas de ondas'''
        self._check_compatibility(other)
        y_res = [y1 + y2 for y1, y2 in zip(self.y, other.y)]
        return Waveform(self.x, y_res)


def sum_waveforms_1(wf1, wf2):
    ''' Soma duas formas de ondas'''
    # Checando se os eixos x são compatíveis
    if wf1.x != wf2.x:
        print("Deu problema, os eixos x não batem!")
        raise Exception
    # Inicializando o novo eixo y
    y_res = []
    # Somando cada ponto
    for i in range(len(wf1.x)):
        y_res.append(wf1.y[i] + wf2.y[i])
    return Waveform(wf1.x, y_res)


def sum_waveforms_2(wf1, wf2):
    ''' Soma duas formas de ondas'''
    # Checando se os eixos x são compatíveis
    if wf1.x != wf2.x:
        raise Exception("Deu problema, os eixos x não batem!")
    # Inicializando o novo eixo y
    y_res = []
    # Somando cada ponto
    for i, y1 in enumerate(wf1.y):
        y_res.append(y1 + wf2.y[i])
    return Waveform(wf1.x, y_res)


def sum_waveforms_3(wf1, wf2):
    ''' Soma duas formas de ondas'''
    # Checando se os eixos x são compatíveis
    if wf1.x != wf2.x:
        raise Exception("Deu problema, os eixos x não batem!")
    # Inicializando o novo eixo y
    y_res = []
    # Somando cada ponto
    for y1, y2 in zip(wf1.y, wf2.y):
        y_res.append(y1 + y2)
    return Waveform(wf1.x, y_res)


def sum_waveforms_4(wf1, wf2):
    ''' Soma duas formas de ondas'''
    # Checando se os eixos x são compatíveis
    if wf1.x != wf2.x:
        raise Exception("Deu problema, os eixos x não batem!")
    y_res = [y1 + y2 for y1, y2 in zip(wf1.y, wf2.y)]
    return Waveform(wf1.x, y_res)


if __name__ == "__main__":
    x = [0, 1, 2, 3, 4, 5]
    y = [0, 1, 4, 9, 16, 25]
    wf1 = Waveform(x, y)

    wf2 = Waveform(x, [1, 2, 3, 4, 5, 6])
    wf3 = sum_waveforms_1(wf1, wf2)
    wf3.plot()
