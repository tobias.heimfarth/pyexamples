import numpy as np

# vectors
a = np.array([0., 1., 2., 3., 4.])
b = np.linspace(2., 10., 5)
print(a)
print(b)
# Operations
print(a+b)
print(a*b)
print(3*a**2)

# indexes
print(a[2])
print(a[:3])
print(a[2:])
