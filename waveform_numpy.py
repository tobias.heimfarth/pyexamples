import matplotlib.pyplot as plt
import numpy as np


class Waveform:
    """
    Waveforms...
    """

    def __init__(self, x0, dx, y):
        """
        Test...
        """
        self.x0 = x0
        self.dx = dx
        self.y = np.array(y)

    @property
    def length(self):
        """
        Number of points in the waveform.
        """
        return len(self.y)

    @property
    def x(self):
        """
        x-axis values
        """
        x_max = self.x0 + (self.length-1)*self.dx
        return np.linspace(self.x0, x_max, self.length)

    def plot(self):
        # https://matplotlib.org/stable/tutorials/pyplot.html
        plt.plot(self.x, self.y)
        plt.show()

    def __add__(self, other):
        """
        Adds two waveforms or a waveform with a constant.
        """
        if isinstance(other, Waveform):
            return Waveform(self.x0, self.dx, self.y + other.y)
        else:
            return Waveform(self.x0, self.dx, self.y + other)
