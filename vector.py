def mod(x, y):
    '''Calcula o módulo de um vetor'''
    return x**2 + y**2


res = mod(5, 10)
print(res)


def sum_vectors(x1, y1, x2, y2):
    ''' Calcula as componentes da soma de 2 vetores'''
    return x1+x2, y1+y2


res = sum_vectors(5, 10, -3, 6)
print(res)

# Definição de um tipo novo -> Vetor


class Vetor:
    def __init__(self, x, y):
        self.x = x
        self.y = y


# Criacao de uma instância de Vetor (um vetor particular)
v = Vetor(5, 10)

print(v)


def mod(v):
    return v.x**2 + v.y**2


print(mod(v))


class Vetor_2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def mod(self):
        return self.x**2 + self.y**2


v = Vetor_2(5, 10)
print(v.mod())
